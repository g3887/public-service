<?php
    include 'koneksi/koneksi.php';
    error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="asset/css/login.css">
    <title>Login | Perpustakaan</title>
</head>
<body id="bg-login">
    <div class="background">
        <img class="background" src="https://wallpapercave.com/wp/wp4676578.jpg" alt="thumbnail">
    </div>
     
    <div class="main-content">
        <div class="image-main-login-content">
            <img class="image-login-content" src="https://www.marketingdonut.co.uk/sites/default/files/why-technology-alone-wont-improve-your-customer-service243533209.jpg" alt="thumbnail">
        </div>
        <div class="login-box"> 
            <h2>- MEMBER LOGIN -</h2>
            <form action="" method="POST">
                <input type="text" name="user" placeholder="Username" class="input-control">
                <input type="password" name="pass" placeholder="Password" class="input-control">
                <input type="submit" name="submit" value="login" class="input-btn">
            </form>
            <?php
                if(isset($_POST['submit'])){
                    $user = mysqli_real_escape_string($conn,$_POST['user']);
                    $pass = mysqli_real_escape_string($conn,$_POST['pass']);

                    $cek = mysqli_query($conn, "SELECT * FROM tb_masyarakat WHERE username = '".$user."' AND password = '".MD5($pass)."'");
                    if(mysqli_num_rows($cek) > 0){
                        $d = mysqli_fetch_object($cek);
                        $_SESSION['status_login'] = true;
                        $_SESSION['masyarakat_global'] = $d;
                        $_SESSION['id'] = $d->id_masyarakat;
                        header('location:masyarakat/index.php');
                    }else{
                        echo '<script>alert("Username atau Password yang dimaksukan salah!!")</script>';
                    }
                }
            ?>
            <hr>
            <div class="randomtxt1">- Or Sign Up with -</div>
            <div class="login_with_google">Google</div>
            <div class="login_with_facebook">Facebook</div>
            <hr>
            <div class="signup_link">
                <a class="link" href="registration.php">Dont have an account? Sign Up</a>
            </div>
            <div class="signup_link">
                <a class="link" href="operator_login.php">Login as Operator</a>
            </div>
        </div>
    </div>

</body>
</html>