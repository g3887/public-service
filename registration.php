<?php
    include "koneksi/koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="asset/css/Registration.css">
    <title>Document</title>
</head>
<body id="bg-login">
    <div class="background">
        <img class="background" src="https://wallpapercave.com/wp/wp4676578.jpg" alt="">
    </div> 
    <div class="registration-content">
        <div class="registration_side_content">
            <img class="regist_content_background" src="https://i.pinimg.com/originals/32/0a/c3/320ac3391a8b76472288402c570af156.jpg" alt="">
        </div>
        <div class="registration-box">
            <form action="" method="POST">
                <h2>Sign Up</h2>
                <h3>We Will Need..</h3>
                <div class="control1">
                    <input type="text" name="name" class="input-control" placeholder="Your Name..." required>   
                </div>
                <input type="text" name="username" class="input-control" placeholder="Your Username..." required>
                <input type="email" name="email" class="input-control" placeholder="Your Email..." required>
                <div class="name_telp_input_control">
                    <div class="control3">
                        <input type="password" name="password1"  class="input-control3" placeholder="Your Password..." required>
                    </div>
                    <div class="control3">
                        <input type="password" name="password2"  class="input-control3" placeholder="One More Time ?..." required>      
                    </div>    
                </div>
                <div class="create-btn">
                    <input type="submit" name="create" value="Sign up" placeholder="Your Name..." class="input-btn">
                </div>
            </form>
            <div class="have_account">
                <p><a href="login.php">Already have account?</a></p>
            </div>
            <?php
                if(isset($_POST['create'])){
                    $name = $_POST['name'];
                    $username = $_POST['username'];
                    $email = $_POST['email'];
                    $pass1 = $_POST['password1'];
                    $pass2 = $_POST['password2'];
                    
                    if($pass2 != $pass1){
                        echo '<script>alert("Incorect Password In Reapeat Password")</script>';
                    }else{
                        $insert = mysqli_query($conn, "INSERT INTO tb_masyarakat VALUES(
                                        null,
                                        '".$name."',
                                        '".$username."',
                                        '".$email."',   
                                        '".MD5($pass1)."')");
                        if($insert){
                            echo '<script>alert("Add Data Succesfull")</script>';
                            echo '<script>window.location="masyarakat/index.php"</script>';
                        }else{
                            echo 'Failed'.mysqli_error($conn);  
                        }
                    }
                }
            ?>
        </div>
        <div class="other_signup">
            <div class="selection_other">
                <h3>Also You Can...</h3>
                <div class="selection_1">
                    <p><span class="else">login in</span> Google</p>
                </div>
                <div style="margin-top:-10px;" class="selection_1">
                    <p><span class="else">login in</span> Facebook</p>
                </div>
            </div>
            <div class="extra">
                <p>OR</p>
            </div>
        </div>
    </div>
</body>
</html>